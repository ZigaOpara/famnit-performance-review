-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:52745
-- Generation Time: Sep 13, 2020 at 06:40 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `localdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `goal_id` int(11) DEFAULT NULL,
  `review_id` int(11) DEFAULT NULL,
  `content` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `user` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `draft` tinyint(1) DEFAULT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `goal_id`, `review_id`, `content`, `type`, `user`, `draft`, `date`) VALUES
(13, 7, NULL, 'ok', 2, 'Žiga Opara', NULL, '2020-09-13 13:11:40'),
(14, 9, NULL, 'amazing', 2, 'Žiga Opara', NULL, '2020-09-13 13:11:47'),
(15, 10, NULL, 'can\'t say', 2, 'Žiga Opara', NULL, '2020-09-13 13:11:54'),
(16, NULL, 5, 'pretty good', 4, 'Žiga Opara', NULL, '2020-09-13 13:12:00'),
(17, 7, NULL, 'Better than expected', 3, 'Klen Copic', NULL, '2020-09-13 13:12:19'),
(18, 9, NULL, 'Agreed', 3, 'Klen Copic', NULL, '2020-09-13 13:12:27'),
(19, NULL, 5, 'Perfect!', 5, 'Klen Copic', NULL, '2020-09-13 13:12:46');

-- --------------------------------------------------------

--
-- Table structure for table `comment_type`
--

CREATE TABLE `comment_type` (
  `id` int(11) NOT NULL,
  `title` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment_type`
--

INSERT INTO `comment_type` (`id`, `title`) VALUES
(1, 'GoalsReject'),
(2, 'EmployeeAssessment'),
(3, 'SupperiorAssessment'),
(4, 'EmployeeFinal'),
(5, 'SupperiorFinal');

-- --------------------------------------------------------

--
-- Table structure for table `goal`
--

CREATE TABLE `goal` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `review_id` int(11) NOT NULL,
  `employee_assessment` int(11) DEFAULT NULL,
  `superior_assessment` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `goal`
--

INSERT INTO `goal` (`id`, `title`, `description`, `review_id`, `employee_assessment`, `superior_assessment`, `type`) VALUES
(7, 'Goal 1', 'goal description 1', 5, 3, 4, 1),
(9, 'goal 2', 'desc 2', 5, 5, 5, 1),
(10, '3', '3!', 5, 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `goal_type`
--

CREATE TABLE `goal_type` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `goal_type`
--

INSERT INTO `goal_type` (`id`, `title`) VALUES
(1, 'Custom'),
(2, 'Predefined');

-- --------------------------------------------------------

--
-- Table structure for table `rating_scale`
--

CREATE TABLE `rating_scale` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rating_scale`
--

INSERT INTO `rating_scale` (`id`, `title`) VALUES
(0, 'NotApplicable'),
(1, 'Unsatisfactory'),
(2, 'MeetsSomeExpectations'),
(3, 'MeetsExpectations'),
(4, 'ExceedExpectations'),
(5, 'Outstanding');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `user_guid` varchar(50) NOT NULL,
  `final_assessment` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `year`, `status`, `user_guid`, `final_assessment`) VALUES
(5, 2020, 6, 'f11d4f03-a814-4cbe-8e65-fc4d4ed30e71', 5),
(6, 2020, 1, '62b51fda-395b-4601-86fc-ba867925de34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `review_status`
--

CREATE TABLE `review_status` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `review_status`
--

INSERT INTO `review_status` (`id`, `title`) VALUES
(1, 'PendingGoalsDefinition'),
(2, 'CustomGoalsDefined'),
(3, 'SuperiorGoalsConfirmed'),
(4, 'EnployeeAssessment'),
(5, 'SuperiorAssessment'),
(6, 'ManagementAssessment'),
(7, 'NoReview');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `goal_fkey` (`goal_id`),
  ADD KEY `review_fkey` (`review_id`),
  ADD KEY `comment_type_fkey` (`type`);

--
-- Indexes for table `comment_type`
--
ALTER TABLE `comment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goal`
--
ALTER TABLE `goal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_id_fkey` (`review_id`),
  ADD KEY `employee_assessment_fkey` (`employee_assessment`),
  ADD KEY `superior_assessment_fkey` (`superior_assessment`),
  ADD KEY `type_fkey` (`type`);

--
-- Indexes for table `goal_type`
--
ALTER TABLE `goal_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating_scale`
--
ALTER TABLE `rating_scale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `final_assessment_fkey` (`final_assessment`),
  ADD KEY `status_fkey` (`status`);

--
-- Indexes for table `review_status`
--
ALTER TABLE `review_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `comment_type`
--
ALTER TABLE `comment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `goal`
--
ALTER TABLE `goal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `goal_type`
--
ALTER TABLE `goal_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rating_scale`
--
ALTER TABLE `rating_scale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `review_status`
--
ALTER TABLE `review_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_type_fkey` FOREIGN KEY (`type`) REFERENCES `comment_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `goal_fkey` FOREIGN KEY (`goal_id`) REFERENCES `goal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `review_fkey` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `goal`
--
ALTER TABLE `goal`
  ADD CONSTRAINT `employee_assessment_fkey` FOREIGN KEY (`employee_assessment`) REFERENCES `rating_scale` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `review_id_fkey` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `superior_assessment_fkey` FOREIGN KEY (`superior_assessment`) REFERENCES `rating_scale` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `type_fkey` FOREIGN KEY (`type`) REFERENCES `goal_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `final_assessment_fkey` FOREIGN KEY (`final_assessment`) REFERENCES `rating_scale` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `status_fkey` FOREIGN KEY (`status`) REFERENCES `review_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
