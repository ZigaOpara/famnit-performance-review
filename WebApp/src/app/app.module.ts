import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorModalComponent } from './components/error-modal/error-modal.component';
import { ReviewEditComponent } from './components/review-edit/review-edit.component';
import { JwtInterceptor } from './infrastructure/jwt.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RatingComponent } from './components/shared/rating/rating.component';
import { GoalListEditComponent } from './components/goal-list-edit/goal-list-edit.component';
import { GoalEditComponent } from './components/goal-edit/goal-edit.component';
import { EmployeesReviewComponent } from './components/employees-review/employees-review.component';
import { CommentTypePipe } from './pipes/commentType.pipe';
import { ReviewStatusPipe } from './pipes/reviewStatus.pipe';
import { PanelComponent } from './components/shared/panel/panel.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AveragePipe } from './pipes/average.pipe';
import { StepperComponent } from './components/shared/stepper/stepper.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ErrorModalComponent,
    ReviewEditComponent,
    GoalListEditComponent,
    RatingComponent,
    GoalEditComponent,
    EmployeesReviewComponent,
    CommentTypePipe,
    ReviewStatusPipe,
    PanelComponent,
    AveragePipe,
    StepperComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  entryComponents: [
    ErrorModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
