import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GoalService} from 'src/app/services/goal.service';
import {RatingScale} from 'src/app/models/ratingScale';
import {Goal} from 'src/app/models/goal';
import {ReviewStatus} from 'src/app/models/review';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {CommentService} from 'src/app/services/comment.service';
import {Comment, CommentType} from 'src/app/models/comment';


@Component({
  selector: 'pr-goal-edit',
  templateUrl: './goal-edit.component.html',
  styleUrls: ['./goal-edit.component.scss']
})
export class GoalEditComponent implements OnInit {

  @Input() goal: Goal;
  @Input() status: ReviewStatus;
  @Input() superiorView = false;
  @Output() delete = new EventEmitter<Goal>();
  @Output() update = new EventEmitter<Goal>();
  @Output() updateComment = new EventEmitter<Comment>();
  @Output() removeComment = new EventEmitter<any>();
  reviewStatus = ReviewStatus;
  assessmentForm: FormGroup;
  editGoalForm: FormGroup;
  editCommentForm: FormGroup;
  modalEditGoal: NgbModalRef;
  modalEditComment: NgbModalRef;
  currentComment: Comment;
  comment = '';

  constructor(private goalService: GoalService, private commentService: CommentService, private formBuilder: FormBuilder, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.editGoalForm = this.formBuilder.group({
      title: this.formBuilder.control(this.goal.title, Validators.required),
      description: this.formBuilder.control(this.goal.description)
    });

    this.editCommentForm = this.formBuilder.group({
      content: this.formBuilder.control(this.currentComment, Validators.required)
    });

    let rate;
    // Modifies goal object by adding 1 to goal rate if it exists, to show correct status of rate on rate component
    if (this.status === ReviewStatus.SuperiorGoalsConfirmed) {
      if (this.goal.employeeAssessment !== undefined && this.goal.employeeAssessment !== null) {
        rate = this.goal.employeeAssessment + 1;
      } else {
        rate = null;
      }
    } else {
      if (this.goal.superiorAssessment !== undefined && this.goal.superiorAssessment !== null) {
        rate = this.goal.superiorAssessment + 1;
      } else {
        rate = null;
      }
    }

    this.assessmentForm = this.formBuilder.group({
      rate: this.formBuilder.control(rate, Validators.required),
      comment: this.formBuilder.control(null, Validators.required)
    });
  }

  openEditGoalModal(content) {
    this.editGoalForm.controls.title.setValue(this.goal.title);
    this.editGoalForm.controls.description.setValue(this.goal.description);
    this.editGoalForm.markAsPristine();
    this.modalEditGoal = this.modalService.open(content, {backdrop: 'static', size: 'xl'});
  }

  editGoal() {
    if (this.editGoalForm.invalid) {
      return;
    }
    this.goalService.updateGoal({...this.goal, ...this.editGoalForm.value}).subscribe(() => {
      this.update.emit(this.goal);
      this.modalEditGoal.close();
    });
  }

  deleteGoal() {
    this.goalService.deleteGoal(this.goal.id).subscribe(() => {
      this.delete.emit(this.goal);
    });
  }

  addAssessment(rating: RatingScale) {
    if (this.status === this.reviewStatus.SuperiorGoalsConfirmed) {
      this.goalService.addEmployeeAssessment(this.goal.id, rating).subscribe(() => {
      });
    } else {
      this.goalService.addSuperiorAssessment(this.goal.id, rating).subscribe(() => {
      });
    }
  }

  openEditCommentModal(content, comment: Comment) {
    this.currentComment = comment;
    this.editCommentForm.controls.content.setValue(this.currentComment.content);
    this.editCommentForm.markAsPristine();
    this.modalEditComment = this.modalService.open(content, {backdrop: 'static', size: 'xl'});
  }

  addComment() {
    const content = this.assessmentForm.controls.comment.value;
    const newComment = {content, goalId: this.goal.id} as Comment;
    if (this.status === ReviewStatus.SuperiorGoalsConfirmed) {
      newComment.type = CommentType.EmployeeAssessment;
    } else {
      newComment.type = CommentType.SuperiorAssessment;
    }
    this.commentService.addComment(newComment).subscribe((comment) => {
      this.goal.comments.push(comment);
      this.assessmentForm.controls.comment.reset();
    });
  }

  editComment() {
    if (this.editCommentForm.invalid) {
      return;
    }
    this.commentService.updateComment({...this.currentComment, ...this.editCommentForm.value}).subscribe(() => {
      this.updateComment.emit();
      this.modalEditComment.close();
    });
  }

  deleteComment(comment: Comment) {
    this.commentService.deleteComment(comment.id).subscribe(() => {
      this.removeComment.emit();
    });
  }

  isValid(): boolean {
    this.assessmentForm.markAllAsTouched();
    return this.assessmentForm.controls.rate.valid;
  }

  isUpdatable(commentType: CommentType): boolean {
    switch (commentType) {
      case CommentType.EmployeeAssessment:
        return this.status === ReviewStatus.SuperiorGoalsConfirmed && !this.superiorView;
      case CommentType.SuperiorAssessment:
        return this.superiorView;
      default:
        return false;
    }
  }
}
