import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ReviewService} from 'src/app/services/review.service';
import {Review, ReviewStatus} from 'src/app/models/review';
import {Observable, Subscription} from 'rxjs';
import {GoalListEditComponent} from '../goal-list-edit/goal-list-edit.component';
import {User} from 'src/app/models/user';
import {ContextService} from 'src/app/services/context.service';
import {ProgressService} from 'src/app/services/progress.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RatingScale} from 'src/app/models/ratingScale';
import {Comment, CommentType} from 'src/app/models/comment';
import {CommentService} from 'src/app/services/comment.service';
import {PdfService} from '../../services/pdf.service';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'pr-review-edit',
  templateUrl: './review-edit.component.html',
  styleUrls: ['./review-edit.component.scss']
})

export class ReviewEditComponent implements OnInit, OnDestroy {
  @Input() superiorView = false;
  @Input() employeeStream: Observable<User>;
  @Input() employee;
  @ViewChild('goalEdit', {static: false}) goalEdit: GoalListEditComponent;
  @ViewChild('pdf', {static: false}) pdf: ElementRef;
  @Output() statusChanged = new EventEmitter<ReviewStatus>();
  finalAssessmentForm: FormGroup;
  employeeFinalCommentForm: FormGroup;
  reviewStatus = ReviewStatus;
  review: Review;
  errorList = [];
  editCommentForm: FormGroup;
  modalEditComment: NgbModalRef;
  currentComment: Comment;
  steps;
  private employeeSubscription: Subscription;
  private contextChangeSubscription: Subscription;
  private employeeGuid = null;

  // byteArray: Byte;

  constructor(private reviewService: ReviewService, private commentService: CommentService,
              private contextService: ContextService, private progressService: ProgressService,
              private formBuilder: FormBuilder, private pdfService: PdfService, private modalService: NgbModal) {
    this.steps = this.buildStatusArray();
  }

  ngOnInit() {
    if (!this.superiorView) {
      this.loadData();
    } else {
      this.employeeSubscription = this.employeeStream.subscribe((user) => {
        this.employeeGuid = user.guid;
        this.loadData();
      });
    }
    this.contextChangeSubscription = this.contextService.contextChangeStream.subscribe(() => {
      this.loadData();
    });

    this.editCommentForm = this.formBuilder.group({
      content: this.formBuilder.control(this.currentComment, Validators.required)
    });

    this.finalAssessmentForm = this.formBuilder.group({
      rate: this.formBuilder.control(null, Validators.required),
      comment: this.formBuilder.control(null, Validators.required)
    });

    this.employeeFinalCommentForm = this.formBuilder.group({
      comment: this.formBuilder.control(null, Validators.required)
    });
  }

  ngOnDestroy(): void {
    if (this.employeeSubscription) {
      this.employeeSubscription.unsubscribe();
    }
    if (this.contextChangeSubscription) {
      this.contextChangeSubscription.unsubscribe();
    }
  }

  loadData() {
    this.progressService.startProgress();
    if (!this.superiorView) {
      this.reviewService.getReview(this.contextService.year).subscribe(review => {
        this.loadReview(review);
        this.errorList = [];
      });
    } else {
      this.reviewService.getEmployeeReview(this.contextService.year, this.employeeGuid).subscribe(review => {
        this.loadReview(review);
        this.errorList = [];
      });
    }
  }

  private loadReview(review: Review) {
    this.review = review;
    this.progressService.stopProgress();
    if (review) {
      this.finalAssessmentForm.controls.rate.setValue(this.review.finalAssessment);
    }
  }

  addFinalAssessment(rating: RatingScale) {
    this.reviewService.addFinalAssessment(this.review.id, rating).subscribe(() => {
    });
  }

  addComment(type: string) {
    let content = '';
    if (type === 'S') {
      content = this.finalAssessmentForm.controls.comment.value;
    } else if (type === 'E') {
      content = this.employeeFinalCommentForm.controls.comment.value;
    }
    const newComment = {content, reviewId: this.review.id} as Comment;
    if (this.superiorView) {
      newComment.type = CommentType.SuperiorFinal;
    } else {
      newComment.type = CommentType.EmployeeFinal;
    }
    this.commentService.addComment(newComment).subscribe((comment) => {
      this.review.comments.push(comment);
      this.finalAssessmentForm.controls.comment.reset();
    });
  }

  openEditCommentModal(content, comment: Comment) {
    this.currentComment = comment;
    this.editCommentForm.controls.content.setValue(this.currentComment.content);
    this.editCommentForm.markAsPristine();
    this.modalEditComment = this.modalService.open(content, {backdrop: 'static', size: 'xl'});
  }

  editComment() {
    if (this.editCommentForm.invalid) {
      return;
    }
    this.commentService.updateComment({...this.currentComment, ...this.editCommentForm.value}).subscribe(() => {
      // this.updateComment.emit();
      this.loadData();
      this.modalEditComment.close();
    });
  }

  deleteComment(comment: Comment) {
    this.commentService.deleteComment(comment.id).subscribe(() => {
      this.loadData();
    });
  }

  statusChange(reviewId: number, newStatus: ReviewStatus) {
    let errors;
    if (newStatus === ReviewStatus.ManagementAssessment) {
      errors = this.validate();
    } else {
      errors = this.goalEdit.validate(newStatus);
    }
    if (errors.length > 0) {
      this.errorList = errors;
      return;
    } else {
      this.errorList = [];
    }
    this.reviewService.changeStatus(reviewId, newStatus).subscribe(() => {
      this.loadData();
      this.statusChanged.emit(newStatus);
    });
  }

  statusSet(reviewId: number, newStatus: ReviewStatus) {
    const status = ReviewStatus[newStatus];
    // @ts-ignore
    this.reviewService.changeStatus(reviewId, status).subscribe(() => {
      this.loadData();
      this.statusChanged.emit();
    });
  }

  validate(): string[] {
    const errors = [];
    this.finalAssessmentForm.markAllAsTouched();
    if (!this.finalAssessmentForm.controls.rate.valid) {
      errors.push('Validation error in final assessment');
    }
    return errors;
  }

  generatePdf() {
    this.pdfService.getPdf(this.review).subscribe((response) => {
      const linkSource = 'data:application/pdf;base64,' + response;
      const downloadLink = document.createElement('a');
      const fileName = `Review-${this.employee.displayName.replace(' ', '_')}-${this.review.year}.pdf`;
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
    });
  }

  isUpdatable(comment: Comment): boolean {
    switch (comment.type) {
      case CommentType.SuperiorFinal:
        return this.superiorView;
      case CommentType.EmployeeFinal:
        return this.review.status === ReviewStatus.SuperiorGoalsConfirmed && !this.superiorView;
      default:
        return false;
    }
  }

  StringIsNumber = value => isNaN(Number(value)) === false;

  buildStatusArray() {
    return Object.keys(ReviewStatus)
      .filter(this.StringIsNumber)
      .map(key => ReviewStatus[key]);
  }
}
