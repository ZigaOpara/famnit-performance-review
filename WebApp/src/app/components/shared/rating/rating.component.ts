import {Component, OnInit, Output, EventEmitter, Input, forwardRef} from '@angular/core';
import {RatingScale} from 'src/app/models/ratingScale';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'pr-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => RatingComponent),
    }
  ]
})
export class RatingComponent implements OnInit, ControlValueAccessor {

  @Input() rate: RatingScale;
  @Input() readonly = false;
  @Input() small = false;
  @Output() rateChange = new EventEmitter<RatingScale>();

  currentRate: number;
  onChange: any;

  constructor() {
  }

  ngOnInit() {
    if (this.rate !== undefined && this.rate !== null) {
      this.currentRate = this.rate + 1;
    } else {
      this.currentRate = 0;
    }
  }

  getToolTip(index: number): string {
    switch (index) {
      case 1:
        return 'Unsatisfactory';
      case 2:
        return 'Meets some but not all expectations';
      case 3:
        return 'Meets expectations';
      case 4:
        return 'Exceed expectations';
      case 5:
        return 'Outstanding';
    }
  }

  onRateChange() {
    if (this.currentRate > 0) {
      this.rateChange.emit(this.currentRate - 1);
      this.onChange(this.currentRate - 1);
    }
  }

  writeValue(obj: any): void {
    this.currentRate = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
    this.readonly = isDisabled;
  }
}
