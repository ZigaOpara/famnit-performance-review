import {Component, Input, OnInit} from '@angular/core';
import {Review, ReviewStatus} from '../../../models/review';

@Component({
  selector: 'pr-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {
  @Input() review: Review;
  steps;

  constructor() {
    this.steps = this.buildStatusArray();
  }

  ngOnInit() {
  }

  buildStatusArray(): object[] {
    const array = Object.keys(ReviewStatus)
      .map(key => ({id: key, name: ReviewStatus[key]}));
    return array.slice(0, array.length / 2 - 1);
  }

}
