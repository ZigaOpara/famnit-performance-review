import {Component, OnInit} from '@angular/core';
import {UserService} from 'src/app/services/user.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Login} from 'src/app/models/login';
import {Router, ActivatedRoute} from '@angular/router';
import {VersionService} from '../../services/version.service';

@Component({
  selector: 'pr-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  returnUrl: string;
  error = '';
  version = '';

  constructor(private userService: UserService, private versionService: VersionService,
              private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) {
    if (this.userService.currentUser) {
      this.router.navigate(['/']).then(() => null);
    }
  }

  ngOnInit() {
    this.getVersion();
    this.loginForm = this.formBuilder.group({
      username: this.formBuilder.control(null, Validators.required),
      password: this.formBuilder.control(null, Validators.required)
    });

    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  get controls() {
    return this.loginForm.controls;
  }

  getVersion() {
    this.versionService.getVersion().subscribe(
      v => {
        this.version = v;
      }
    );
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;

    const login = this.loginForm.value as Login;
    this.userService.login(login).subscribe(
      () => {
        this.router.navigate([this.returnUrl]).then(() => null);
      },
      error => {
        this.error = error.error.Message;
        this.loading = false;
      });
  }
}
