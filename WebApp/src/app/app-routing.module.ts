import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ReviewEditComponent } from './components/review-edit/review-edit.component';
import { EmployeesReviewComponent } from './components/employees-review/employees-review.component';
import { AuthGuard } from './infrastructure/auth.guard';


const routes: Routes = [
  { path: 'reviewEmployees', component: EmployeesReviewComponent, canActivate: [AuthGuard], data: { superior: true }},
  { path: 'review', component: ReviewEditComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: 'review' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
