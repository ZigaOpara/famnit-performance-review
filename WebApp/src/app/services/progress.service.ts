import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProgressService {

  private actionInProgress: boolean;
  private timeouts: any[] = [];

  constructor() { }
  startProgress(waitTime?: number): any {
    const timeout = setTimeout(() => {
      this.actionInProgress = true;
    }, waitTime ? waitTime : 50);
    this.timeouts.push(timeout);
    return timeout;
  }

  stopProgress(timeout?: any) {
    if (!timeout) {
      this.timeouts.forEach(t => {
        clearTimeout(t);
      });
      this.timeouts = [];

      this.actionInProgress = false;
    } else {
      clearTimeout(timeout);
      const index = this.timeouts.findIndex(x => x === timeout);
      if (index !== -1) {
        this.timeouts.splice(index, 1);
      }
      if (this.timeouts.length === 0) {

        this.actionInProgress = false;
      }
    }
  }
  public get isActionInProgress(): boolean {
    return this.actionInProgress;
  }
}
