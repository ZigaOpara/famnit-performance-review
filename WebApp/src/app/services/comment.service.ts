import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ErrorHandlerService} from './error-handler.service';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Comment} from '../models/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private readonly rootUrl = 'api/comment';

  private readonly httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient, private errorService: ErrorHandlerService) {
  }

  addComment(comment: Comment): Observable<Comment> {
    return this.http.post<Comment>(this.rootUrl, comment, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  updateComment(comment: Comment): Observable<Comment> {
    return this.http.put<Comment>(this.rootUrl, comment, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  deleteComment(id: number): Observable<boolean> {
    return this.http.delete<boolean>(`${this.rootUrl}/${id}`, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }
}
