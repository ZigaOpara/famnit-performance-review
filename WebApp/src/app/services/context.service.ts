import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContextService {

  private contextChangeSource: Subject<number>;
  contextChangeStream: Observable<any>;

  constructor() {
    this.contextChangeSource = new Subject<any>();
    this.contextChangeStream = this.contextChangeSource.asObservable();
  }

  get year() {
    const year = localStorage.getItem('contextYear');
    if (!year) {
      this.year = new Date().getFullYear();
      return this.year;
    }
    return +year;
  }

  set year(year: number) {
    localStorage.setItem('contextYear', year.toString());
    this.contextChangeSource.next();
  }

  getContextYearList(): number[] {
    const startYear = 2019;
    const currentYear = new Date().getFullYear();
    const yearList = [];
    for (let year = startYear; year <= currentYear; year++) {
      yearList.push(year);
    }
    return yearList;
  }
}
