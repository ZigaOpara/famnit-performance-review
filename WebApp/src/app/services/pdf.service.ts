import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Review} from '../models/review';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PdfService {
  private readonly rootUrl = 'api/pdf';

  private readonly httpOptions = {
    responseType: 'text/plain' as 'json'
  };

  constructor(private http: HttpClient) {
  }

  getPdf(review: Review): Observable<any> {
    return this.http.post<any>(this.rootUrl, review, this.httpOptions).pipe();
  }
}
