import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'avg'})
export class AveragePipe implements PipeTransform {
  transform(items: any, attr: string): any {
    if (items) {
      const na = items.filter(a => a[attr] === 0).length;
      const sum = items.reduce((a, b) => a + b[attr], 0);
      return sum / (items.length - na);
    }
  }
}
