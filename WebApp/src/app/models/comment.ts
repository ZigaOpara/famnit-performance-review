export class Comment {
    id: number;
    goalId: number;
    reviewId: number;
    content: string;
    type: CommentType;
    date: Date;
    user: string;
}

export enum CommentType {
    GoalsReject = 1,
    EmployeeAssessment = 2,
    SuperiorAssessment = 3,
    EmployeeFinal = 4,
    SuperiorFinal = 5,
}
