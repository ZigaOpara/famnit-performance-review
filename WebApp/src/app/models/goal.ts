import { RatingScale } from './ratingScale';
import { Comment } from './comment';

export class Goal {
    id: number;
    reviewId: number;
    title: string;
    description: string;
    type: GoalType;
    employeeAssessment: RatingScale;
    superiorAssessment: RatingScale;
    comments: Comment[];
}

export enum GoalType {
    Custom = 1,
    Predefined = 2
}
