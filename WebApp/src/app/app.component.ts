import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from './models/user';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ContextService } from './services/context.service';
import { ProgressService } from './services/progress.service';

@Component({
  selector: 'pr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {

  currentUser: User;
  userSubscription: Subscription;

  constructor(private router: Router, private userService: UserService, public contextService: ContextService, public progressService: ProgressService) { }

  ngOnInit(): void {
    this.userSubscription = this.userService.userStream.subscribe(user => {
      this.currentUser = user;
      if (user == null) {
        // this.router.navigate(['/login']);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

  logout() {
    this.userService.logout();
  }

  contextYearChange(year: number) {
    this.contextService.year = year;
  }
}

