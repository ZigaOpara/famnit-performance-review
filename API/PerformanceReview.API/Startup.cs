
using System;
using System.Data;
using System.Text;
using Dapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Npgsql;
using PerformanceReview.API.Infrastructure;
using PerformanceReview.Business;
using PerformanceReview.Business.Infrastructure;
using PerformanceReview.Business.Repositories;
using PerformanceReview.Business.Services;

namespace PerformanceReview.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddHttpContextAccessor();
            services.AddMemoryCache();
            services.AddCors();

            //Configuration
            ADConfig adConfig = new ADConfig();
            Configuration.GetSection("ActiveDirectory").Bind(adConfig);
            JwtConfig jwtConfig = new JwtConfig();
            Configuration.GetSection("Jwt").Bind(jwtConfig);
            ADCredentials adCredentials = new ADCredentials();
            Configuration.GetSection("ActiveDirectoryCredentials").Bind(adCredentials);

            //Configuration - Create singleton from instance
            services.AddSingleton(adConfig);
            services.AddSingleton(jwtConfig);
            services.AddSingleton(adCredentials);

#if DEBUG
            services.AddTransient(_ => new AppDb("Server=localhost;Port=3306;Database=performancereview;Uid=admin;Pwd=123;"));
#else
            services.AddTransient(_ => new AppDb(Environment.GetEnvironmentVariable("MYSQLCONNSTR_localdb")));
#endif

            //JWT authentication
            var key = Encoding.ASCII.GetBytes(jwtConfig.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            //Configure DI
            //Service
            services.AddScoped<IGoalService, GoalService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<IUserService, MockUserService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IPdfService, PdfService>();

            //Repository
            services.AddScoped<IGoalRepository, GoalRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            
            SqlMapper.AddTypeHandler(new MySqlGuidTypeHandler());
            SqlMapper.RemoveTypeMap(typeof(Guid));
            SqlMapper.RemoveTypeMap(typeof(Guid?));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
