﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Services;

namespace PerformanceReview.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {

        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpPost]
        public IActionResult InsertGoal([FromBody] Comment comment)
        {
            return new JsonResult(_commentService.InsertComment(comment));
        }

        [HttpPut]
        public IActionResult UpdateComment([FromBody] Comment comment)
        {
            return new JsonResult(_commentService.UpdateComment(comment));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteComment([FromRoute] int id)
        {
            return new JsonResult(_commentService.DeleteComment(id));
        }
    }
}