﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace PerformanceReview.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IConfiguration Configuration;
    
        public TestController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
    
        [HttpGet]
        public IActionResult GetBooks()
        {
            var constr = Environment.GetEnvironmentVariable("MYSQLCONNSTR_localdb");
            return Ok(constr);
        }
    }
}