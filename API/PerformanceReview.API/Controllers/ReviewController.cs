﻿using System;
using System.Text.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Services;

namespace PerformanceReview.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpGet("{year}")]
        public IActionResult GetReview(int year)
        {
            return new JsonResult(_reviewService.GetReview(year));
        }

        [HttpPost("changestatus")]
        public void ChangeStatus([FromBody]JsonElement data)
        {
            _reviewService.ChangeStatus(data.GetProperty("reviewId").GetInt32(), (ReviewStatus)data.GetProperty("status").GetInt32());
        }

        [HttpGet("employeereview/{year}/{id}")]
        public IActionResult GetEmployeeReview(int year, Guid id)
        {
            return new JsonResult(_reviewService.GetEmployeeReview(year, id));
        }

        [HttpPost("finalassessment")]
        public void AddSuperiorAssessment([FromBody]JsonElement data)
        {
            _reviewService.AddFinalAssessment(data.GetProperty("reviewId").GetInt32(), (RatingScale)data.GetProperty("rating").GetInt32());
        }

        [HttpGet("employeesreviewstatus/{year}")]
        public IActionResult GetEmployeesReviewStatus(int year)
        {
            return new JsonResult(_reviewService.GetEmployeesReviewStatus(year, new Guid(HttpContext.User.Identity.Name)));
        }
    }
}