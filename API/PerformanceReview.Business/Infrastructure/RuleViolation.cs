﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PerformanceReview.Business.Infrastructure
{
    public class RuleViolation
    {
        private string Identifier { get; }
        public string Message { get; }

        public RuleViolation(string message)
            : this(string.Empty, message) { }

        public RuleViolation(Expression<Func<object>> property, string message)
            : this(GetPath(property), message) { }

        private RuleViolation(string identifier, string message)
        {
            Message = message;
            Identifier = string.IsNullOrEmpty(identifier) ? "_" : identifier;
        }

        public override string ToString()
        {
            return $"{Identifier}: {Message}";
        }

        private static string GetPath(Expression<Func<object>> expr)
        {
            var stack = new Stack<string>();

            MemberExpression me;
            switch (expr.Body.NodeType)
            {
                case ExpressionType.Convert:
                case ExpressionType.ConvertChecked:
                    me = ((expr.Body is UnaryExpression ue) ? ue.Operand : null) as MemberExpression;
                    break;
                default:
                    me = expr.Body as MemberExpression;
                    break;
            }

            while (me != null)
            {
                stack.Push(me.Member.Name);
                me = me.Expression as MemberExpression;
            }

            var skip = 0;
            if (stack.Count > 1)
                skip = 1;

            return string.Join("_", stack.Skip(skip).ToArray());
        }
    }
}
