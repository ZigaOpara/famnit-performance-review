﻿using System;
using System.Collections.Generic;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Repositories
{
    public interface IReviewRepository
    {
        Review GetReview(int reviewId);
        Review GetReviewByYear(Guid userGuid, int year);
        Review InsertReview(Guid userGuid, Review review);
        void UpdateStatus(int reviewId, ReviewStatus status);
        void AddFinalAssessment(int id, RatingScale rating);
        IEnumerable<Review> GetReviewList(int year, List<Guid> userGuids);
    }
}
