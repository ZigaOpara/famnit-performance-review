﻿using System.Linq;
using Dapper;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly AppDb _db;

        public CommentRepository(AppDb db)
        {
            _db = db;
        }

        public Comment InsertComment(Comment comment)
        {
            var result = _db.Connection.Query<int>(@"INSERT INTO comment (goal_id, review_id, content, type, user, date) 
                                                VALUES (@goalId, @reviewId, @content, @type, @user, @date);
                                         SELECT LAST_INSERT_ID();",
                new
                {
                    goalId = comment.GoalId,
                    reviewId = comment.ReviewId,
                    content = comment.Content,
                    type = comment.Type,
                    user = comment.User,
                    date = comment.Date,
                }).Single();
            comment.Id = result;
            return comment;
        }

        public Comment UpdateComment(Comment comment)
        {
            var res = _db.Connection.Execute(@"UPDATE comment 
                                SET
                                   content = @content,
                                   date = @date
                                WHERE id = @id", new
            {
                id = comment.Id,
                content = comment.Content,
                date = comment.Date,
            });
            comment.Id = res;
            return comment;
        }

        public bool DeleteComment(int id)
        {
            _db.Connection.Execute(@"DELETE FROM comment 
                                WHERE id = @index", new
            {
                index = id
            });
            return true;
        }
    }
}