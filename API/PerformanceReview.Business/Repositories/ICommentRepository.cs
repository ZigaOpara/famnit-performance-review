﻿using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Repositories
{
    public interface ICommentRepository
    {
        Comment InsertComment(Comment comment);
        Comment UpdateComment(Comment comment);
        bool DeleteComment(int id);
    }
}
