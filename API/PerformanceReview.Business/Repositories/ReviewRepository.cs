﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Repositories
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly AppDb _db;

        public ReviewRepository(AppDb db)
        {
            _db = db;
        }

        public Review GetReview(int reviewId)
        {
            var reviewDictionary = new Dictionary<int, Review>();
            return _db.Connection.Query<Review, Comment, Review>(@"SELECT R.id, R.year, R.status, R.user_guid, R.final_assessment,
                                                              C.id, C.goal_id, C.review_id, C.content, C.type, C.date, C.date, C.user
                                                       FROM review AS R
                                                       LEFT JOIN comment AS C ON R.Id = C.review_id                              
	                                                   WHERE R.id= @id",
                (review, comment) =>
                {
                    Review reviewEntity = null;
                    if (review.Id != null)
                    {
                        if (!reviewDictionary.TryGetValue(review.Id.Value, out reviewEntity))
                        {
                            reviewDictionary.Add(review.Id.Value, reviewEntity = review);
                            reviewEntity.Comments = new List<Comment>();
                        }
                    }

                    if (comment != null)
                    {
                        reviewEntity?.Comments.Add(comment);
                    }

                    return reviewEntity;
                }, new {id = reviewId}).FirstOrDefault();
        }

        public Review GetReviewByYear(Guid userGuid, int year)
        {
            var reviewDictionary = new Dictionary<int, Review>();
            try
            {
                return _db.Connection.Query<Review, Comment, Review>(@"SELECT R.id, R.year, R.status, R.user_guid as userGuid, R.final_assessment as finalAssessment,
                                                              C.id, C.goal_id, C.review_id, C.content, C.type, C.date, C.date, C.user
                                                       FROM review AS R
                                                       LEFT JOIN comment AS C ON R.Id = C.review_id                              
	                                                   WHERE R.year = @year AND R.user_guid = @userGuid",
                    (review, comment) =>
                    {
                        Review reviewEntity = null;
                        if (review.Id != null)
                        {
                            if (!reviewDictionary.TryGetValue(review.Id.Value, out reviewEntity))
                            {
                                reviewDictionary.Add(review.Id.Value, reviewEntity = review);
                                reviewEntity.Comments = new List<Comment>();
                            }
                        }

                        if (comment != null)
                        {
                            reviewEntity?.Comments.Add(comment);
                        }

                        return reviewEntity;
                    }, new {year, userGuid}).FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

        public Review InsertReview(Guid userGuid, Review review)
        {
            var result = _db.Connection.Query<int>(@"INSERT INTO review (year, status, user_guid) 
                                         VALUES (@year, @status, @userGuid);
                                         SELECT LAST_INSERT_ID();",
                new {year = review.Year, status = review.Status, userGuid}).Single();
            review.Id = result;
            return review;
        }

        public void UpdateStatus(int reviewId, ReviewStatus status)
        {
            _db.Connection.Execute(@"UPDATE review SET status = @status
                         WHERE id = @id",
                new {id = reviewId, status});
        }

        public void AddFinalAssessment(int id, RatingScale rating)
        {
            _db.Connection.Execute(@"UPDATE review SET final_assessment = @rating
                         WHERE id = @id",
                new {id, rating});
        }

        public IEnumerable<Review> GetReviewList(int year, List<Guid> userGuids)
        {
            return _db.Connection.Query<Review>(@"SELECT * FROM review
                                      WHERE year = @year AND user_guid IN @userGuids",
                new {year, userGuids}).ToList();
        }
    }
}