﻿using System;
using System.Collections.Generic;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Services
{
    public interface IUserService
    {
        User Authenticate(Login login);
        List<User> GetEmployeeList(Guid superiorGuid);
        User GetUserDetail(Guid userGuid); 
    }
}
