﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Repositories;

namespace PerformanceReview.Business.Services
{
    public class GoalService : IGoalService
    {
        private readonly IGoalRepository _goalRepository;

        public GoalService(IGoalRepository goalRepository)
        {
            _goalRepository = goalRepository;
        }

        public Goal GetGoal(int id)
        {
            return _goalRepository.GetGoal(id);
        }
        public List<Goal> GetGoalList(int reviewId)
        {
            var res = _goalRepository.GetGoalList(reviewId);
            res.ForEach(goal =>
            {
                goal.Description = Regex.Replace(goal.Description, @"\s{16,}", "\n");
                goal.Comments.ForEach(comment =>
                    comment.Content = Regex.Replace(comment.Content, @"\s{16,}", "\n"));
            });
            
            return res;
        }

        public void DeleteGoal(int id)
        {
            _goalRepository.DeleteGoal(id);
        }

        public Goal InsertGoal(Goal goal)
        {
            return _goalRepository.InsertGoal(goal);
        }

        public void UpdateGoal(Goal goal)
        {
            _goalRepository.UpdateGoal(goal);
        }

        public void AddEmployeeAssessment(int id, RatingScale rating)
        {
            _goalRepository.AddEmployeeAssessment(id, rating);
        }

        public void AddSuperiorAssessment(int id, RatingScale rating)
        {
            _goalRepository.AddSuperiorAssessment(id, rating);
        }
    }
}
