﻿﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using PerformanceReview.Business.Infrastructure;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Services
{
    public class MockUserService : IUserService
    {
        private readonly JwtConfig _jwtConfig;

        public MockUserService(JwtConfig jwtConfig)
        {
            _jwtConfig = jwtConfig;
        }
        
        public User Authenticate(Login login)
        {
            User user;
            LoginValidation(login);

            try
            {
                user = users.Find(x => x.Username == login.Username && x.Password == login.Password);
                user.IsSuperior = IsSuperior(user.Guid);
            }
            catch (Exception)
            {
                throw new Exception("Username or password is incorrect");
            }
            
            AddJwtToken(user);
            return user;
        }
        
        private void AddJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.Guid.ToString()),
                }),
                Expires = DateTime.UtcNow.AddHours(3),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
        }
        
        private static void LoginValidation(Login login)
        {
            var errs = new List<RuleViolation>();
            if (string.IsNullOrWhiteSpace(login.Username))
                errs.Add(new RuleViolation(() => login.Username, "Required"));
            if (string.IsNullOrWhiteSpace(login.Password))
                errs.Add(new RuleViolation(() => login.Password, "Required"));
            if (errs.Count > 0)
                throw new RuleViolationException(errs);
        }

        public List<User> GetEmployeeList(Guid superiorGuid)
        {
            return users.FindAll(x => Equals(x.ManagerGuid, superiorGuid));
        }

        public User GetUserDetail(Guid userGuid)
        {
            return users.FirstOrDefault(x => x.Guid == userGuid);
        }
        
        private bool IsSuperior(Guid userGuid)
        {
            return users.FindAll(x => Equals(x.ManagerGuid, userGuid)).Count > 0;
        }

        private readonly List<User> users = new List<User>
        {
            new User
            {
                Username = "manager",
                Password = "1234",
                Guid = new Guid("62b51fda-395b-4601-86fc-ba867925de34"),
                DisplayName = "Klen Čopič",
                Role = ""
            },
            new User
            {
                Username = "user1",
                Password = "1234",
                Guid = new Guid("f11d4f03-a814-4cbe-8e65-fc4d4ed30e71"),
                ManagerGuid = new Guid("62b51fda-395b-4601-86fc-ba867925de34"),
                DisplayName = "Žiga Opara",
                Role = ""
            },
            new User
            {
                Username = "user2",
                Password = "1234",
                Guid = new Guid("8474ac5d-4af0-42d2-8490-da1446d61991"),
                ManagerGuid = new Guid("62b51fda-395b-4601-86fc-ba867925de34"),
                DisplayName = "Kevin McCallister",
                Role = ""
            },
            new User
            {
                Username = "user3",
                Password = "1234",
                Guid = new Guid("7335cde6-44f0-4762-bc52-21f018079475"),
                ManagerGuid = new Guid("62b51fda-395b-4601-86fc-ba867925de34"),
                DisplayName = "Ethan Hunt",
                Role = ""
            },
        };
    }
}