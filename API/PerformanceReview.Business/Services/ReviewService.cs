﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using PerformanceReview.Business.Infrastructure;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Repositories;

namespace PerformanceReview.Business.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;
        private readonly IGoalService _goalService;
        private readonly IUserService _userService;
        private readonly Guid _userGuid;

        public ReviewService(IReviewRepository reviewRepository, IGoalService goalService, IUserService userService,
            IHttpContextAccessor httpContextAccessor)
        {
            _reviewRepository = reviewRepository;
            _goalService = goalService;
            _userService = userService;
            _userGuid = new Guid(httpContextAccessor.HttpContext.User.Identity.Name);
        }

        public Review GetReview(int year)
        {
            var review = _reviewRepository.GetReviewByYear(_userGuid, year) ?? _reviewRepository.InsertReview(_userGuid,
                             new Review
                             {
                                 Year = year, Status = ReviewStatus.PendingGoalsDefinition, UserGuid = _userGuid
                             });
            return review;
        }

        public Review GetEmployeeReview(int year, Guid userGuid)
        {
            if (_userService.GetUserDetail(userGuid).ManagerGuid != this._userGuid)
                throw new UnauthorizedException();
            return _reviewRepository.GetReviewByYear(userGuid, year);
        }

        public void ChangeStatus(int reviewId, ReviewStatus status)
        {
            using var transaction = new TransactionScope();

            _reviewRepository.UpdateStatus(reviewId, status);
        }

        public void AddFinalAssessment(int id, RatingScale rating)
        {
            _reviewRepository.AddFinalAssessment(id, rating);
        }

        public List<Tuple<Guid, ReviewStatus>> GetEmployeesReviewStatus(int year, Guid userGuid)
        {
            var users = _userService.GetEmployeeList(userGuid).Select(x => x.Guid).ToList();
            var reviews = _reviewRepository.GetReviewList(year, users);
            return reviews.Select(x => new Tuple<Guid, ReviewStatus>(x.UserGuid, x.Status)).ToList();
        }
    }
}