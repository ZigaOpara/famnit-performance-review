﻿using System;
using Microsoft.AspNetCore.Http;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Repositories;

namespace PerformanceReview.Business.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CommentService(ICommentRepository commentRepository, IUserService userService, IHttpContextAccessor httpContextAccessor)
        {
            _commentRepository = commentRepository;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
        }

        public Comment InsertComment(Comment comment)
        {
            comment.Date = DateTime.Now;
            comment.User = _userService.GetUserDetail(new Guid(_httpContextAccessor.HttpContext.User.Identity.Name)).DisplayName;
            return _commentRepository.InsertComment(comment); 
        }

        public Comment UpdateComment(Comment comment)
        {
            comment.Date = DateTime.Now;
            return _commentRepository.UpdateComment(comment);
        }

        public bool DeleteComment(int id)
        {
            return _commentRepository.DeleteComment(id);
        }
    }
}
