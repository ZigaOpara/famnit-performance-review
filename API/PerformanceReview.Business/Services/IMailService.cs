﻿using System.Threading.Tasks;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Services
{
    public interface IMailService
    {
        Task<bool> SendTestEmail(string password);

        Task<bool> SendNotificationEmail(int reviewId, ReviewStatus status);
    }
}