﻿using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Services
{
    public interface ICommentService
    {
        Comment InsertComment(Comment comment);
        Comment UpdateComment(Comment comment);
        bool DeleteComment(int id);
    }
}
