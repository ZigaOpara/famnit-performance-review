﻿namespace PerformanceReview.Business.Models
{
    public enum CommentType
    {
        GoalsReject = 1,
        EmployeeAssessment = 2,
        SuperiorAssessment = 3,
        EmployeeFinal = 4,
        SuperiorFinal = 5,
    }
}
