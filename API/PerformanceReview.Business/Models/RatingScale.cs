﻿namespace PerformanceReview.Business.Models
{
    public enum RatingScale
    {
        NotApplicable = 0,
        Unsatisfactory = 1,
        MeetsSomeExpectations = 2,
        MeetsExpectations = 3,
        ExceedExpectations = 4,
        Outstanding = 5
    }
}
