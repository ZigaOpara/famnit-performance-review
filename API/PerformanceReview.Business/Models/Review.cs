﻿using System;
using System.Collections.Generic;

namespace PerformanceReview.Business.Models
{
    public class Review
    {
        public int? Id { get; set; }
        public Guid UserGuid { get; set; }
        public ReviewStatus Status { get; set; }
        public int Year { get; set; }
        public RatingScale? FinalAssessment { get; set; }
        public List<Comment> Comments { get; set; }
    }
}
